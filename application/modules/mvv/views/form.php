<div class="container mt-3" id="mvv">
    <div class="card">


    <div class="card-header"><h4>Cadastro Missão, Visão e Valores</h4></div>



  <div class="view overlay">
    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2821%29.jpg" alt="Card image cap" style=" width: 400px; position: relative;  margin-left: 350px;">
    <a>
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>


        
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4" enctype="multipart/form-data">
                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <input type="text" name="missao" value="<?= isset($dados['missao']) ? $dados['missao'] : '' ?>" class="form-control" placeholder="Missão..." required>
                    </div>

                </div>
                <div class="form-row mb-4">
                <div class="col-md-12">
                        <input type="text" name="visao" value="<?= isset($dados['visao']) ? $dados['visao'] : '' ?>" class="form-control" placeholder="Visão..." required>
                    </div>
                </div>
                <div class="form-row mb-4">
                <div class="col-md-12">
                        <input type="text" name="valor" value="<?= isset($dados['valor']) ? $dados['valor'] : '' ?>" class="form-control" placeholder="Valor..." required>
                    </div>
                </div>
                
                <div class="text-center text-md-right">
                    <button class="btn btn-warning btn-block" type="submit">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>