<nav class="navbar navbar-expand-md navbar-dark warning-color">
    <a class="navbar-brand" href="#">Formulário para Cadastrar Missão, Visão e Valor</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
               <a class="nav-link waves-effect waves-light" href="<?php echo base_url('mvv') ?>">Cadastrar <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light" href="<?php echo base_url('Mvv/listar') ?>">Visualizar Cadastro</a>
            </li>
    </div>
</nav>