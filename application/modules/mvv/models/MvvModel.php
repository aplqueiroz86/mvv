<?php
include_once APPPATH.'modules\mvv\libraries\MvvLibrarie.php';
class MvvModel extends CI_Model{

    
    public function salvar(){
        $missao   = $this->input->post('missao');
        $visao = $this->input->post('visao');
        $valor = $this->input->post('valor');
       
        if($missao && $visao  && $valor)
        {
                    
            $dados = new MvvLibrarie($missao,$visao,$valor);
            $dados->save();
            redirect('mvv/listar');
        }
        

    }


    public function lista_editar(){
        $html = '';
        $dados = new MvvLibrarie();
       
        $data = $dados->getAll();
        $html .= '<table class="table mx-auto justify-content-center" >';
        $html .= '<tr><th scope="col" class="justify-content-center" >Missão</th><th scope="col" >Visão</th><th scope="col" >Valor</th><th scope="col">Alterar</th></tr>';
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<th scope="col">'.$row['missao'].'</th>';
            $html .= '<th scope="col">'.$row['visao'].'</th>';
            $html .= '<th scope="col">'.$row['valor'].'</th>';
            $html .='<th scope="col">'.$this->icones($row['id']).'</th></tr>';
            }
                $html .= '</table>';
                return $html;
        }

        private function icones($id){
            $html = '';
            $html .= '<a href="'.base_url('mvv/edit/'.$id).'"><i class="far fa-edit mr-3 text-primary"></i></a>';
            $html .= '<a href="'.base_url('mvv/delete/'.$id).'"><i class="far fa-trash-alt text-danger"></i></a>';
            return $html;

        }
        
        public function carrega_dados($id){
            $dados = new MvvLibrarie();
            return $dados->getById($id);
        }

        public function atualizar($id){
            if(sizeof($_POST) == 0) return;
    
            $data = $this->input->post();
            $dados = new MvvLibrarie();
            if($dados->update($data, $id))
                redirect('mvv/listar');    
        }

        public function delete($id){
            $dados = new MvvLibrarie();
            $dados->delete($id);
        }

}
